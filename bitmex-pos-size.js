// window.location.reload();

(function increaseSideBarWidth() {
  var sidebar = document.querySelector(".sidebar");
  sidebar.style.maxWidth = "30rem";
  sidebar.parentNode.style.transition = "flex-basis 0.25s ease-in";
  sidebar.parentNode.style.flexBasis = "30rem";
})();

var stopOrder = {
  pegOffsetValue: 0,
  displayQty: null,
  ordType: "Stop",
  postOnlyExecInst: true,
  lastPriceExecInst: true,
  indexPriceExecInst: false,
  closeExecInst: true,
  reduceOnlyExecInst: true,
  timeInForce: "GoodTillCancel",
};

var entryOrder = {
  pegOffsetValue: 0,
  displayQty: null,
  ordType: "Limit",
  postOnlyExecInst: true,
  lastPriceExecInst: true,
  indexPriceExecInst: false,
  closeExecInst: true,
  reduceOnlyExecInst: false,
  timeInForce: "GoodTillCancel",
};

var targetOrder = {
  closeExecInst: true,
  displayQty: null,
  indexPriceExecInst: false,
  lastPriceExecInst: true,
  ordType: "LimitIfTouched",
  pegOffsetValue: 0,
  postOnlyExecInst: true,
  reduceOnlyExecInst: true,
  timeInForce: "GoodTillCancel",
};

var injectOrderSet = () => {
  var riskPercent = 0.01;
  var getStopPrice = () => BitMEX.getData().orderInput.stopPx;
  var getEntryPrice = () => BitMEX.getData().orderInput.price;
  var targetPrice = null;

  var SATOSHIS_PER_BTC = 100000000;

  var getLastPriceInUsd = () =>
    parseFloat(
      document
        .querySelectorAll(".lastPriceWidget")[1]
        .querySelector(".priceWidget").innerText
    );

  var getAvailableBalanceInSatoshis = () =>
    parseFloat(
      document
        .querySelectorAll("._39qDSUxb")[1]
        .nextSibling.innerText.match(/\d+/g)
        .join("")
    );
  var getAvailableBalanceInBitcoin = () =>
    getAvailableBalanceInSatoshis() / SATOSHIS_PER_BTC;
  var getAvailableBalanceInUSD = () =>
    getAvailableBalanceInBitcoin() * getLastPriceInUsd();

  var getRiskAmount = (
    availableBalance = getAvailableBalanceInUSD(),
    riskPercentage = riskPercent
  ) => availableBalance * riskPercentage;

  var getPriceDistance = (from, to) => (from - to) / from;
  var getStopLossDistanceFromEntry = () =>
    getPriceDistance(getEntryPrice(), getStopPrice());

  var getPositionSize = (
    riskAmount = getRiskAmount(),
    distanceToStopLoss = getStopLossDistanceFromEntry()
  ) => Math.floor(riskAmount / distanceToStopLoss);

  var updatePlaceOrderPanelStyles = () => {
    var $panelPlaceOrder = document.querySelectorAll(".orderWrapper")[0];
    $panelPlaceOrder.style = `
          background: var(--sidebarBackground2);
          margin-left: -5px;
          margin-right: -5px;
          padding: 5px 5px 0;
          border-top: 1px solid rgba(51,51,51,0.35);
          border-bottom: 1px solid rgba(51,51,51,0.35);
      `;
  };

  updatePlaceOrderPanelStyles();

  var addPanelOrderSet = (getOrderSetMarkup = getOrderSetMarkup) => {
    var $panelPlaceOrder = document.querySelectorAll(".orderWrapper")[0];

    $panelPlaceOrder.insertAdjacentHTML("beforebegin", getOrderSetMarkup());
  };

  var getOrderSetMarkup = () => `
      <li class="accordion-group orderWrapper open notAnimating"><h4 class="accordion-toggle"> <i class="fa fa-fw fa-chevron-right fa-rotate-90"></i> Order set</h4><ul class="panel-collapse collapse open in" style="height: auto;"><div class="measuringWrapper"><div class="borderWrapper"><div class="controlsBody limit"><div class="orderControlsInputs inputs "><div class="numInputs clearfix"><div class="risk orderControlsInput"><div class="inputWrap"><label class="nowrap tooltipWrapper hovered" for="risk">Risk</label><span class="input-group-inline"><span class="input-group-addon">%</span><input step=".01" placeholder="" name="risk" max="3" min="0.5" class="form-control valid" autocomplete="off" id="risk" tabindex=1 type="number" value="1"> <span class="input-group-addon" id="riskAmount" style="width:320px">20,000 XBt</span></span></div></div><div id="orderSetWrapper" style=" display: flex; flex-direction: column; "><div id="targetPriceWrapper" class="price orderControlsInput" style="transition: transform 0.25s cubic-bezier(0.65, -0.34, 0.29, 1.15) 0s;"><div class="inputWrap"><label class="nowrap tooltipWrapper" for="targetPrice" style=" border-left: 2px solid #56bc76; ">Target Price</label><span class="input-group-inline"><span class="input-group-addon">USD</span><input type="number" id="targetPrice" autocomplete="off" class="form-control valid" min="-Infinity" max="100000000" name="price" placeholder="" required="" step="0.5" tabindex="1"></span></div></div><div class="price orderControlsInput"><div class="inputWrap"><label for="entryPrice" class="nowrap tooltipWrapper" style=" border-left: 2px solid white; ">Limit Price</label><span class="input-group-inline"><span class="input-group-addon">USD</span><input type="number" id="entryPrice" autocomplete="off" class="form-control valid" min="-Infinity" max="100000000" name="price" placeholder="" required="" step="0.5" tabindex="3"></span></div></div><div id="stopPriceWrapper" class="price orderControlsInput" style="transition: transform 0.25s cubic-bezier(0.65, -0.34, 0.29, 1.15) 0s;"><div class="inputWrap"><label for="stopPrice" class="nowrap tooltipWrapper" style=" border-left: 2px solid #e5603b; ">Stop Price</label><span class="input-group-inline"><span class="input-group-addon">USD</span><input type="number" id="stopPrice" autocomplete="off" class="form-control valid" min="-Infinity" max="100000000" name="price" placeholder="" required="" step="0.5" tabindex="4" autofocus></span></div></div></div><div class="qty orderControlsInput"><div class="inputWrap"><label for="orderSetQty" class="nowrap tooltipWrapper hovered">Quantity</label><span class="input-group-inline"><span class="input-group-addon">USD</span><input step="1" placeholder="" name="orderSetQty" max="100000000" min="-Infinity" class="form-control valid" autocomplete="off" id="orderSetQty" type="number" disabled=""></span></div></div></div><div class="inputsRow row orderControlsButtons "><div class="btnWrapper nowrap "><button class="btn-lg btn btn-block btn-success" id="btnPlaceOrder"><label class="nowrap" id="lblPlaceOrder">Buy / Long</label><div id="orderSetQtyPrice" class="summary">78 @ 4500</div></button></div><span class="tooltipWrapper"><div class="cost nowrap" id="orderCost"></div></span></div></div><div class="buySellInfo"><div class="lineItem"><span class="tooltipWrapper"><span class="key">Order Value</span><span class="value" id="orderValue"></span></span></div><div class="lineItem"><a href="/app/wallet" class="noColor"><span class="tooltipWrapper"><span class="key">Available Balance</span><span class="value" id="availableBalance"></span></span></a></div></div></div></div></div></ul></li>
  `;

  addPanelOrderSet(getOrderSetMarkup);

  var updatePositionSizeUI = () => {
    if (riskPercent && getStopPrice() && getEntryPrice()) {
      var positionSize = getPositionSize();
      var $btnPlaceOrder = document.querySelector("#btnPlaceOrder");

      $btnPlaceOrder.classList.remove(
        "btn-primary",
        "btn-success",
        "btn-danger"
      );
      $btnPlaceOrder.disabled = false;

      if (positionSize !== Infinity) {
        var absolutePositionSize = Math.abs(positionSize);
        document.querySelector("#orderSetQty").value = absolutePositionSize;
        document.querySelector("#orderSetQtyPrice").innerText =
          absolutePositionSize + " @ " + getEntryPrice();

        if (positionSize > 0) {
          $btnPlaceOrder.classList.add("btn-success");
          $btnPlaceOrder.classList.add("btn-success");
          document.querySelector("#lblPlaceOrder").innerText = "Buy / Long";
          document.querySelector("#targetPriceWrapper").style.transform = null;
          document.querySelector("#stopPriceWrapper").style.transform = null;
          document.querySelector("#stopPrice").tabIndex = 4;
          document.querySelector("#targetPrice").tabIndex = 2;
        } else if (positionSize < 0) {
          $btnPlaceOrder.classList.add("btn-danger");
          document.querySelector("#lblPlaceOrder").innerText = "Sell / Short";
          document.querySelector("#targetPriceWrapper").style.transform =
            "translateY(66px)";
          document.querySelector("#stopPriceWrapper").style.transform =
            "translateY(-66px)";
          document.querySelector("#stopPrice").tabIndex = 2;
          document.querySelector("#targetPrice").tabIndex = 4;
        }

        BitMEX.actions.changeOrderInput({ orderQty: absolutePositionSize });
      } else {
        $btnPlaceOrder.classList.add("btn-primary");
        document.querySelector("#lblPlaceOrder").innerText = "Place order";
        document.querySelector("#orderSetQtyPrice").innerText =
          "Stop too close to entry!";
        $btnPlaceOrder.disabled = true;
        BitMEX.actions.changeOrderInput({ orderQty: 0 });
      }
    }
  };

  document.querySelector("#risk").addEventListener("change", (e) => {
    riskPercent = parseFloat(e.target.value) / 100;
    document.querySelector("#riskAmount").innerText =
      getRiskAmount(getAvailableBalanceInSatoshis()).toLocaleString() + " XBt";
    setTimeout(updatePositionSizeUI, 50);
  });

  document.querySelector("#targetPrice").addEventListener("change", (e) => {
    targetPrice = parseFloat(e.target.value);
    setTimeout(updatePositionSizeUI, 50);
  });

  document.querySelector("#entryPrice").addEventListener("change", (e) => {
    BitMEX.actions.changeOrderInput({
      price: parseFloat(e.target.value),
    });
    setTimeout(updatePositionSizeUI, 50);
  });

  document.querySelector("#stopPrice").addEventListener("change", (e) => {
    BitMEX.actions.changeOrderInput({
      stopPx: parseFloat(e.target.value),
    });
    setTimeout(updatePositionSizeUI, 50);
  });

  document.querySelector("#btnPlaceOrder").addEventListener("click", () => {
    var posSize = getPositionSize();
    var BUY = "Buy";
    var SELL = "Sell";

    BitMEX.actions.submitOrder({
      payload: {
        ...stopOrder,
        stopPx: getStopPrice(),
        orderQty: Math.abs(posSize),
        side: getEntryPrice() > getStopPrice() ? SELL : BUY,
      },
      validateFn: () => true,
    });

    setTimeout(() => {
      BitMEX.getData().dialogs.forEach((dialog) => {
        BitMEX.actions.dialogConfirm({
          callbackData: {},
          id: dialog.id,
          dontShowAgain: false,
        });
      });
    }, 50);

    BitMEX.actions.submitOrder({
      payload: {
        ...entryOrder,
        price: getEntryPrice(),
        orderQty: Math.abs(posSize),
        side: getEntryPrice() > getStopPrice() ? BUY : SELL,
      },
      validateFn: () => true,
    });

    setTimeout(() => {
      BitMEX.actions.submitOrder({
        payload: {
          ...targetOrder,
          price: targetPrice,
          orderQty: Math.abs(posSize),
          stopPx:
            targetPrice > getEntryPrice() ? targetPrice + 10 : targetPrice - 10,
          side: targetPrice > getEntryPrice() ? SELL : BUY,
        },
        validateFn: () => true,
      });

      setTimeout(() => {
        BitMEX.getData().dialogs.forEach((dialog) => {
          BitMEX.actions.dialogConfirm({
            callbackData: {},
            id: dialog.id,
            dontShowAgain: false,
          });
        });
      }, 50);
    }, 500);

    setTimeout(updatePositionSizeUI, 50);
  });

  setInterval(() => {
    var posSize = getPositionSize();

    if (posSize > 0) {
      document.getElementById(
        "orderCost"
      ).innerText = document.querySelectorAll(".cost")[1].innerText;
    } else if (posSize < 0) {
      document.getElementById(
        "orderCost"
      ).innerText = document.querySelectorAll(".cost")[2].innerText;
    }

    document.getElementById("orderValue").innerText = document.querySelectorAll(
      ".buySellInfo .value"
    )[2].innerText;
    document.getElementById(
      "availableBalance"
    ).innerText = document.querySelectorAll(".buySellInfo .value")[3].innerText;
  }, 25);
};

BitMEX.actions.changeOrderInput({
  price: 0,
  orderQty: 0,
  stopPx: 0,
  ordType: "Limit",
});

BitMEX.actions.positionUpdateLeverage({
  symbol: "XBTUSD",
  leverage: 1,
});

injectOrderSet();
